<?php

namespace Services\Fitlers;

/**
 * Check filter Methods in your repo.
 */
trait FilterHandling
{

    /**
     * @var string $filterEnding Just your filter ending
     */
    protected $filterEnding = 'Filter';

    /**
     * Handele Current Filters
     *
     * @param array $filters Filers data : [filterMethodName => filterValue]
     */
    public function handleFilters(array $filters): void
    {
        foreach ($filters as $filterKey => $filterValue) {
            $filterMethod = $filterKey . $this->filterEnding;
            if ($filterValue) {
                if (method_exists(__CLASS__, $filterMethod)) {
                    $this->$filterMethod($filterValue);
                }
            }
        }

        return;
    }
}
